const API_KEY = "2c8416c27f986f17e0094df9efd173ff";
const BASE_URL = "https://api.openweathermap.org/data/2.5/forecast";

const getForecast = async (city) =>{
    let requestUrl = BASE_URL + `?q=${city}&units=metric&appid=${API_KEY}`;

    const response = await fetch(requestUrl);

    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error("Status Code:"+response.status);
    }
}

getForecast("Mumbai")
    .then(data =>{
        console.log(data);
    })
    .catch(err =>{
        console.log(err)
    });
